use std::collections::HashMap;

use serde::{Serialize, Deserialize};

// TODO: I don't think serde_json is the correct type to use where it's being used, especially
// since it's being exposed in a public API
use serde_json::Value;
use chrono::prelude::*;

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Credential {
    #[serde(rename = "@context")]
    pub context: Vec<String>,
    pub id: String,

    #[serde(rename = "type")]
    pub type_: Vec<String>,

    pub issuer: String,

    pub issuance_date: DateTime<Utc>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub expiration_date: Option<DateTime<Utc>>,

    pub credential_subject: Subject,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub credential_status: Option<Status>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub proof: Option<Proof>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub terms_of_use: Option<Vec<TermsOfUse>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub evidence: Option<Vec<Evidence>>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct TermsOfUse {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    #[serde(rename = "type")]
    pub type_: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Evidence {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,

    #[serde(rename = "type")]
    pub type_: String,

    #[serde(flatten)]
    pub property_set: Option<HashMap<String, Value>>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Subject {
    pub id: String,

    #[serde(flatten)]
    pub property_set: Option<HashMap<String, Value>>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Status {
    pub id: String,

    #[serde(rename = "type")]
    pub type_: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Proof {
    #[serde(rename = "type")]
    pub type_: String,
    pub created: DateTime<Utc>,
    pub proof_purpose: String,
    pub verification_method: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub challenge: Option<String>,
    
    #[serde(skip_serializing_if = "Option::is_none")]
    pub domain: Option<String>,

    pub jws: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Presentation {
    #[serde(rename = "@context")]
    pub context: Vec<String>,

    #[serde(rename = "type")]
    pub type_: String,

    pub verifiable_credential: Vec<Credential>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub proof: Option<Proof>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub holder: Option<String>,
}

