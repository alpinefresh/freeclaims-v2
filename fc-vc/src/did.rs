use std::collections::BTreeMap as Map;

use serde::{Serialize, Deserialize};
use serde_json::{Value, json};
use serde_json;
use chrono::prelude::*;

pub const DEFAULT_CONTEXT: &str = "https://www.w3.org/2019/did/v1";

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Document {
    #[serde(rename = "@context")]
    context: String,
    id: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    created: Option<DateTime<Utc>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    updated: Option<DateTime<Utc>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    authentication: Option<Vec<VerificationMethod>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    service: Option<Vec<Service>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    public_key: Option<Vec<PublicKey>>,

    #[serde(skip_serializing_if = "Option::is_none")]
    controller: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    proof: Option<Proof>,
}

impl Document {
    pub fn new(id: String) -> Document {
        Document {
            context: DEFAULT_CONTEXT.to_string(),
            id: id,
            created: None,
            updated: None,
            authentication: None,
            service: None,
            public_key: None,
            controller: None,
            proof: None,
        }
    }
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PublicKey {
    id: String,
    #[serde(rename = "type")]
    type_: String,
    controller: String,

    #[serde(skip_serializing_if = "Option::is_none")]
    public_key_pem: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    public_key_hex: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    public_key_base58: Option<String>
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[serde(untagged)]
pub enum VerificationMethod {
    Did(String),
    PublicKey(PublicKey),
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Service {
    id: String,
    #[serde(rename = "type")]
    type_: String,
    service_endpoint: String,

    #[serde(flatten)]
    property_set: Option<Map<String, Value>>,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct Proof {
    #[serde(rename = "type")]
    type_: String,
    created: DateTime<Utc>,
    creator: String,
    signature_value: String,
}

pub fn create_example_document() -> String {
    let mut service_ps = Map::new();
    service_ps.insert("description".to_string(),
                      json!("My public social inbox"));
    service_ps.insert("spam_cost".to_string(), json!({
        "amount": "0.50",
        "currency": "USD",
    }));
    let document = Document {
        context: "https://w3id.org/future-method/v1".to_string(),
        id: "did:example:123456789abcdefghi".to_string(),
        created: None,
        updated: None,
        controller: None,
        authentication: Some(vec![
            VerificationMethod::Did("did:example:123456789abcdefghi#keys-1".to_string()),
            VerificationMethod::Did("did:example:123456789abcdefghi#keys-3".to_string()),
            VerificationMethod::PublicKey(PublicKey {
                id: "did:example:123456789abcdefghi#keys-2".to_string(),
                type_: "Ed25519VerificationKey2018".to_string(),
                controller: "did:example:123456789abcdefghi".to_string(),
                public_key_pem: None,
                public_key_base58: Some("H3C2AVvLMv6gmMNam3uVAjZpfkcJCwDwnZn6z3wXmqPV".to_string()),
                public_key_hex: None,
            }),
        ]),

        service: Some(vec![
            Service {
                id: "did:example:123456789abcdefghi#oidc".to_string(),
                type_: "OpenIdConnectVersion1.0Service".to_string(),
                service_endpoint: "https://openid.example.com".to_string(),
                property_set: None,
            },
            Service {
                id: "did:example:123456789abcdefghi#vcStore".to_string(),
                type_: "CredentialRepositoryService".to_string(),
                service_endpoint: "https://repository.example.com/service/8377464".to_string(),
                property_set: None,
            },
            Service {
                id: "did:example:123456789abcdefghi#xdi".to_string(),
                type_: "XdiService".to_string(),
                service_endpoint: "https://xdi.example.com/8377464".to_string(),
                property_set: None,
            },
            Service {
                id: "did:example:123456789abcdefghi#hub".to_string(),
                type_: "HubService".to_string(),
                service_endpoint: "https://hub.example.com/.identity/did:example:0123456789abcdef/".to_string(),
                property_set: None,
            },
            Service {
                id: "did:example:123456789abcdefghi#messaging".to_string(),
                type_: "MessagingService".to_string(),
                service_endpoint: "https://example.com/messages/8377464".to_string(),
                property_set: None,
            },
            Service {
                id: "did:example:123456789abcdefghi#inbox".to_string(),
                type_: "SocialWebInboxService".to_string(),
                service_endpoint: "https://social.example.com/83hfh37dj".to_string(),
                property_set: Some(service_ps),
            },
            Service {
                id: "did:example:123456789abcdefghi#push".to_string(),
                type_: "DidAuthPushModeVersion1".to_string(),
                service_endpoint: "http://auth.example.com/did:example:123456789abcdefghi".to_string(),
                property_set: None,
            },
            Service {
                id: "did:example:123456789abcdefghi#bops".to_string(),
                type_: "BopsService".to_string(),
                service_endpoint: "https://bops.example.com/enterprise/".to_string(),
                property_set: None,
            },
        ]),

        proof: None,

        public_key: Some(vec![
            PublicKey {
                id: "did:example:123456789abcdefghi#keys-1".to_string(),
                type_: "RsaVerificationKey2018".to_string(),
                controller: "did:example:123456789abcdefghi".to_string(),
                public_key_pem: Some("-----BEGIN PUBLIC KEY...END PUBLIC KEY-----\r\n".to_string()),
                public_key_base58: None,
                public_key_hex: None,
            },
            PublicKey {
                id: "did:example:123456789abcdefghi#keys-3".to_string(),
                type_: "Ieee2410VerificationKey2018".to_string(),
                controller: "did:example:123456789abcdefghi".to_string(),
                public_key_pem: Some("-----BEGIN PUBLIC KEY...END PUBLIC KEY-----\r\n".to_string()),
                public_key_base58: None,
                public_key_hex: None,
            },
        ]),
    };

    let serialized = serde_json::to_string_pretty(&document).unwrap();
    return serialized;
}

pub fn create_simple_example_document() -> String {
    let document = Document {
        context: "https://www.w3.org/2019/did/v1".to_string(),
        id: "did:example:123456789abcdefghi".to_string(),
        created: None,
        updated: None,
        controller: None,
        authentication: Some(vec![VerificationMethod::PublicKey(PublicKey {
            id: "did:example:123456789abcdefghi#keys-1".to_string(),
            type_: "RsaVerificationKey2018".to_string(),
            controller: "did:example:123456789abcdefghi".to_string(),
            public_key_pem: Some("-----BEGIN PUBLIC KEY...END PUBLIC KEY-----\r\n".to_string()),
            public_key_base58: None,
            public_key_hex: None,
        })]),
        service: Some(vec![Service {
            id: "did:example:123456789abcdefghi#example.com".to_string(),
            type_: "VerifiableCredentialService".to_string(),
            service_endpoint: "https://example.com/vc/".to_string(),
            property_set: None,
        }]),
        proof: None,
        public_key: None,
    };

    let serialized = serde_json::to_string_pretty(&document).unwrap();
    return serialized;
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn ensure_no_runtime_crash_for_example() {
        create_example_document();
    }

    #[test]
    fn ensure_no_runtime_crash_for_simple_example() {
        create_simple_example_document();
    }
}
