use std::collections::HashMap;

use serde_json::json;

use chrono::prelude::*;
use fc_vc::credential::{Subject, Proof, Credential, Presentation};

fn example_credential() -> Presentation {
    let mut ps = HashMap::new();
    ps.insert("alumniOf".to_string(), json!({
        "id": "did:example:c276e12ec21ebfeb1f712ebc6f1",
        "name": [
            {
                "value": "Example University",
                "lang": "en",
            },
            {
                "value": "Exemple d'Université",
                "lang": "fr",
            },
        ],
    }));
    let cred_subject = Subject {
        id: "did:example:ebfeb1f712ebc6f1c276e12ec21".to_string(),
        property_set: Some(ps),
    };
    let proof = Proof {
        type_: "RsaSignature2018".to_string(),
        created: "2017-06-18T21:19:10Z".parse::<DateTime<Utc>>().unwrap(),
        proof_purpose: "assertionMethod".to_string(),
        verification_method: "https://example.edu/issuers/keys/1".to_string(),
        challenge: None,
        domain: None,
        jws: "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..TCYt5XsITJX1CxPCT8yAV-TVkIEq_PbChOMqsLfRoPsnsgw5WEuts01mq-pQy7UJiN5mgRxD-WUcX16dUEMGlv50aqzpqh4Qktb3rk-BuQy72IFLOqV0G_zS245-kronKb78cPN25DGlcTwLtjPAYuNzVBAh4vGHSrQyHUdBBPM".to_string(),
    };
    let cred = Credential {
        context: vec![
            "https://www.w3.org/2018/credentials/v1".to_string(),
            "https://www.w3.org/2018/credentials/examples/v1".to_string(),
        ],
        id: "http://example.edu/credentials/1872".to_string(),
        type_: vec![
            "VerifiableCredential".to_string(),
            "AlumniCredential".to_string(),
        ],
        issuer: "https://example.edu/issuers/565049".to_string(),
        issuance_date: "2014-11-28T12:00:09Z".parse::<DateTime<Utc>>().unwrap(),
        expiration_date: None,
        credential_subject: cred_subject,
        credential_status: None,
        proof: Some(proof),
        terms_of_use: None,
        evidence: None,
    };
    let presentation_proof = Proof {
        type_: "RsaSignature2018".to_string(),
        created: "2018-09-14T21:19:10Z".parse::<DateTime<Utc>>().unwrap(),
        proof_purpose: "authentication".to_string(),
        verification_method: "did:example:ebfeb1f712ebc6f1c276e12ec21#keys-1".to_string(),
        challenge: Some("1f44d55f-f161-4938-a659-f8026467f126".to_string()),
        domain: Some("4jt78h47fh47".to_string()),
        jws: "eyJhbGciOiJSUzI1NiIsImI2NCI6ZmFsc2UsImNyaXQiOlsiYjY0Il19..kTCYt5XsITJX1CxPCT8yAV-TVIw5WEuts01mq-pQy7UJiN5mgREEMGlv50aqzpqh4Qq_PbChOMqsLfRoPsnsgxD-WUcX16dUOqV0G_zS245-kronKb78cPktb3rk-BuQy72IFLN25DYuNzVBAh4vGHSrQyHUGlcTwLtjPAnKb78".to_string(),
    };
    let presentation = Presentation {
        context: vec![
            "https://www.w3.org/2018/credentials/v1".to_string(),
            "https://www.w3.org/2018/credentials/examples/v1".to_string(),
        ],
        type_: "VerifiablePresentation".to_string(),
        verifiable_credential: vec![cred],
        proof: Some(presentation_proof),
        holder: None,
    };

    return presentation;
}

#[test]
fn test_example_credential() {
    let res = serde_json::to_string_pretty(&example_credential());

    assert!(res.is_ok(), "Unable to serialize example credential: {:?}", res.unwrap_err())
}
