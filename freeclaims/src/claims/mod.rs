use serde::Deserialize;

#[derive(Deserialize, Clone)]
/// Twilio client configuration
pub struct TwilioClientData {
    pub id: String,
    pub auth_token: String,
    pub phone_number: String,
}

#[derive(Deserialize, Clone)]
/// Mailgun client configuration
pub struct MailgunClientData {
    pub email: String,
    pub api_key: String,
}
