use serde::Deserialize;

mod oauth;
use oauth::{GoogleClientData, AmazonClientData, FacebookClientData, MSGraphClientData};

mod claims;
use claims::{TwilioClientData, MailgunClientData};

#[derive(Deserialize, Clone)]
struct ServerConfig {
    // identity endpoint info

    google_client: GoogleClientData,
    amazon_client: AmazonClientData,
    facebook_client: FacebookClientData,
    msgraph_client: MSGraphClientData,

    // claim endpoint info
    
    twilio_client: TwilioClientData,
    mailgun_client: MailgunClientData,

    secrets: ServerSecrets,
    bind_address: String,
    bind_port: u16,
    domain: String,
}

// TODO: encrypt in memory at runtime would be cool
#[derive(Deserialize, Clone)]
struct ServerSecrets {
    session_key: String,
    signing_secret: String,
}

fn main() {
    let _server_config: ServerConfig = toml::from_str(include_str!("../server_config.toml"))
        .expect("unable to parse server config");
}
