use serde::Deserialize;

#[derive(Deserialize, Clone)]
/// Google Oauth client configuration
pub struct GoogleClientData {
    pub id: String,
    pub secret: String,
}

#[derive(Deserialize, Clone)]
/// Amazon Oauth client configuration
pub struct AmazonClientData {
    pub id: String,
    pub secret: String,
}

#[derive(Deserialize, Clone)]
/// Facebook Oauth client configuration
pub struct FacebookClientData {
    pub id: String,
    pub secret: String,
}

#[derive(Deserialize, Clone)]
/// MSGraph Oauth client configuration
pub struct MSGraphClientData {
    pub id: String,
    pub secret: String,
}
